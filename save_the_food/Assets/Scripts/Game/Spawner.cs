﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace SaveTheFood
{

    public class Spawner : MonoBehaviour
    {

        #region public fields, events
        public event Action OnSpawnFinish = () => { };
        public event Action OnEnemySpawn = () => { };
        public event Action<int> OnEnemyDead = (int i) => { };
        #endregion
        #region private fields

        WaveElement[] _wave;
        GameObject _enemyPrefab;
        GameObject _target;

        float _time = 0;
        int _currentEnemy = 0;
        int _countEnemy = 0;
        bool isSpawn = false;
        #endregion

        #region private methods


        private void Update()
        {


            if (GameState.Instance().State != GameStates.PLAY) return;

            if (_currentEnemy >= _wave.Length) return;

            if (_time >= _wave[_currentEnemy].time)
            {
                GameObject enemy = Instantiate(_enemyPrefab, gameObject.transform.position, Quaternion.identity);
                enemy.GetComponent<Enemy>().Init(_wave[_currentEnemy].enemyData, _target);
                enemy.GetComponent<Enemy>().OnDead += EnemyDead;
                OnEnemySpawn();

                _currentEnemy++;
                _time = 0;
          
                return;
            }

            _time += Time.deltaTime;
        }
        void EnemyDead( int coins_)
        {
            _countEnemy--;
            OnEnemyDead(coins_);
            if (_countEnemy <= 0)
            {
                OnSpawnFinish();

            }
        }
        #endregion
        #region public methods
        public void Init(GameObject target_, GameObject enemyPrefab_, WaveElement[] wave_)
        {
            _target = target_;
            _enemyPrefab = enemyPrefab_;
            _wave = wave_;

            _time = 0;
            _currentEnemy = 0;
            _countEnemy = _wave.Length;

            isSpawn = true;
        }
        #endregion
    }
}