﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace SaveTheFood
{
    public enum GameStates { START, PLAY, PAUSE, LEVELSTART, LEVELCOMPLETED,GAMEOVER };

    public class GameState
    {
        #region private fields
        GameStates _state = GameStates.START;

        #endregion

        #region public fields
        static GameState ref_ = null;
        #region events
        public event Action OnStart = () => { };
        public event Action OnGameOver = () => { };
        public event Action OnLevelStart = () => { };
        public event Action OnLevelCompleted = () => { };
        #endregion
        #endregion
        #region public methods, properties
        public static GameState Instance()
        {
            if (ref_ == null)
            {
                ref_ = new GameState();
            }

            return ref_;
        }

        public GameStates State
        {
            get
            {
                return _state;
            }
            set
            {
                _state = value;
                SwitchState(_state);

            }

        }
        #endregion
        #region private methods
        GameState()
        {
            _state = GameStates.PAUSE;
            InitEvents();
        }

        void InitEvents()
        {
         OnStart = () => { };
         OnGameOver = () => { };
        OnLevelStart = () => { };
        OnLevelCompleted = () => { };
    }

        void SwitchState(GameStates state_)
        {
            switch (state_)
            {
                case GameStates.START:
                    OnStart();
                    break;
                case GameStates.GAMEOVER:
                    _state = GameStates.PAUSE;
                    OnGameOver();
                    InitEvents();
                    break;
                case GameStates.LEVELSTART:
                    _state = GameStates.PLAY;
                    OnLevelStart();
                    break;
                case GameStates.PLAY:
                    _state = GameStates.PLAY;
                    break;
                case GameStates.LEVELCOMPLETED:
                    _state = GameStates.LEVELCOMPLETED;
                    OnLevelCompleted();
                    break;
            }
        }
        #endregion
    }

}