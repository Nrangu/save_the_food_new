﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SaveTheFood
{

    public class FoodManager : MonoBehaviour
    {
        #region events
        public event Action<int> OnSetMaxHealth = (int i) => { };
        public event Action<int> OnSetHealth = (int i) => {};
        #endregion
        
        #region private methods

        [SerializeField] GameObject[] _food;
        [SerializeField] FoodData[] _foodData;
        GameObject _currentFood = null;

        static FoodManager _ref = null;
        #endregion

        #region private methods
        private void Awake()
        {
            if (_ref == null)
            {
                _ref = this;
            }else
            {
                Destroy(gameObject);
            }
        }
        private void Start()
        {
            GameState.Instance().OnLevelStart += Init;
        }

        private void Init()
        {
            if (_currentFood != null)
            {
                Destroy(_currentFood);
            }

            int currentLevel = Main.Ref.CurrentLevel;
            if (currentLevel >= _food.Length)
            {
                currentLevel = UnityEngine.Random.Range(0, _food.Length);
            }

            _currentFood = Instantiate(_food[UnityEngine.Random.Range(0, _food.Length)], transform.position, Quaternion.identity);
            _currentFood.GetComponent<Food>().Init(_foodData[currentLevel]);
            OnSetMaxHealth(_foodData[currentLevel].Health);

            _currentFood.GetComponent<Food>().OnSetHealth += OnSetHealth;

        }
        #endregion
        #region public methods
        static public  FoodManager Ref()
        {
            return _ref;
        }
        #endregion
    }
}