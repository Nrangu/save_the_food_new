﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SaveTheFood
{

    public class SpawnerManager : MonoBehaviour
    {

        #region events
        public event Action<int> OnSetTotalEnemies = (int i) => { };
        public event Action OnSpawnEnemy = () => { };
        public event Action<int> OnEnemyDead = (int i) => { };
        #endregion
        #region private fields

        private static SpawnerManager _ref = null;

        [SerializeField] AttackData _attackData;

        [SerializeField] Spawner[] _spawners;

        [SerializeField] GameObject _enemyPrefab;
        [SerializeField] GameObject _target = null;

        int _countSpawnerFinish = 0;

        #endregion
        
        #region private methods
        private void Awake()
        {
            if (_ref == null)
            {
                _ref = this;


            }
            else
            {
                Destroy(this);
            }

        }

        // Start is called before the first frame update
        void Start()
        {
            foreach(Spawner obj in _spawners)
            {
                obj.OnSpawnFinish += SpawnFinish;
                obj.OnEnemySpawn += OnSpawnEnemy;
                obj.OnEnemyDead += OnEnemyDead;
            }
            GameState.Instance().OnLevelStart += InitLevel;
        }

        // Update is called once per frame
        void Update()
        {

        }
        #endregion
        #region public methods
        static public  SpawnerManager Ref()
        {
            return _ref;
        }


        public void SpawnFinish()
        {
            _countSpawnerFinish++;
            if (_countSpawnerFinish == _spawners.Length)
            {
                GameState.Instance().State = GameStates.LEVELCOMPLETED;
            }
        }

        public void InitLevel()
        {
            int totalEnemies = 0;
            _countSpawnerFinish = 0;
            WaveElement[][] waves = _attackData.GetAttackWaves(_spawners.Length,0);
            for(int i = 0; i < _spawners.Length; i++)
            {
                _spawners[i].Init(_target, _enemyPrefab, waves[i]);
                totalEnemies += waves[i].Length;
            }
            OnSetTotalEnemies(totalEnemies);
        }

        public GameObject Target
        {
            get
            {
                return _target;
            }

            set
            {
                _target = value;
            }
        }
        #endregion
    }
}
