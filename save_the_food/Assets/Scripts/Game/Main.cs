﻿
using UnityEngine;
using UnityEngine.UI;
using TMPro;

namespace SaveTheFood
{
    public class Main : MonoBehaviour
    {
        #region private fields
        static Main _ref = null;

        [SerializeField] Slider _sliderHealth;
        [SerializeField] TextMeshProUGUI _totalEnemiesText;
        [SerializeField] TextMeshProUGUI _currentEnemiesText;
        [SerializeField] TextMeshProUGUI _scoreText;
        [SerializeField] TextMeshProUGUI _higherScoreText;
        int _score = 0;
        int _currentEnemies = 0;
        int _curentLevel = 0;
        int _higherScore = 0;
        #endregion

        #region private methods
        private void Awake()
        {
            if (_ref == null)
            {
                _ref = this;

            }
            else
            {
                Destroy(gameObject);
            }
 

            GameState.Instance().OnLevelCompleted += () => { _curentLevel++; };
            GameState.Instance().OnLevelStart += levelStart;
            GameState.Instance().OnGameOver += () => { PlayerPrefs.Save(); };
            GameState.Instance().OnStart += Init;

        }
        void Start()
        {
            FoodManager.Ref().OnSetMaxHealth += SetMaxHealth;
            FoodManager.Ref().OnSetHealth += SetHealth;

            SpawnerManager.Ref().OnSetTotalEnemies += SetTotalEnemies;
            SpawnerManager.Ref().OnSpawnEnemy += SetCurrentEnemies;
            SpawnerManager.Ref().OnEnemyDead += AddCoins;

            GameState.Instance().State = GameStates.START;

        }

        private void Init()
        {


            _higherScore = PlayerPrefs.GetInt("HigherScore", 0);
            _higherScoreText.text = _higherScore.ToString();

            _score = 0;
            _scoreText.text = _score.ToString();

            _currentEnemies = 0;
            _currentEnemiesText.text = _currentEnemies.ToString();

            _totalEnemiesText.text = "0";
                
                }
        private void Update()
        {
            if (Input.GetKey(KeyCode.Escape))
            {
                Application.Quit();
            }
        }

        private void levelStart()
        {
            _currentEnemies = 0;
            _currentEnemiesText.text = _currentEnemies.ToString();
        }
        private void AddCoins(int score_)
        {
            _score += score_;
            _scoreText.text = _score.ToString();
            if (_score > _higherScore)
            {
                _higherScore = _score;
                _higherScoreText.text = _higherScore.ToString();
                PlayerPrefs.SetInt("HigherScore", _higherScore);
            }

        }

        private void SetTotalEnemies(int count_)
        {
            _totalEnemiesText.text = count_.ToString();
        }

        private void SetCurrentEnemies()
        {
            _currentEnemies++;
            _currentEnemiesText.text = _currentEnemies.ToString();
        }

        private void SetMaxHealth(int maxHealth_)
        {
            _sliderHealth.maxValue = maxHealth_;
            SetHealth(maxHealth_);
        }

        private void SetHealth(int health_)
        {
            _sliderHealth.value = health_;
        }



        private void OnApplicationQuit()
        {
            PlayerPrefs.Save();
        }
        private void OnApplicationPause(bool pause)
        {
            PlayerPrefs.Save();
        }


        #endregion
        #region public methods
        static public Main Ref
        {
            get
            {
                return _ref;
            }
        }

        public int CurrentLevel
        {
            get
            {
                return _curentLevel;
            }
        }
        #endregion
        #region public properties
        #endregion
        #region public methods
   
        #endregion

    }
}