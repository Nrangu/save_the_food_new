﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace SaveTheFood
{
    public class MenuGameOver : MonoBehaviour
    {
        #region private methods
        private void Awake()
        {
            GameState.Instance().OnGameOver += () => { gameObject.SetActive(true); };
        }
        private void Start()
        {
            gameObject.SetActive(false);
        }
        #endregion
        #region public methods
        public void RestartClick()
        {

            SceneManager.LoadScene(0);
        }

        public void ExitClick()
        {
            Application.Quit();
        }
        #endregion

    }
}