﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SaveTheFood
{

    public class MenuLevelCompleted : MonoBehaviour
    {
        #region private methods
        // Start is called before the first frame update
        void Start()
        {
            GameState.Instance().OnLevelCompleted += () => { gameObject.SetActive(true); };
            gameObject.SetActive(false);
        }
        #endregion
        #region public methods
        public void Click()
        {
            gameObject.SetActive(false);
            GameState.Instance().State = GameStates.LEVELSTART;
        }
        #endregion
    }
}