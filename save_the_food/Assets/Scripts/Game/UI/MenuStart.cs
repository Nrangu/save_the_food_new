﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SaveTheFood {

    public class MenuStart : MonoBehaviour
    {
        // Start is called before the first frame update
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {

        }

        public void StartClick()
        {
            gameObject.SetActive(false);
            GameState.Instance().State = GameStates.LEVELSTART;
        }
    }
}