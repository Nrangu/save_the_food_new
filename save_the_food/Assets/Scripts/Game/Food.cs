﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SaveTheFood {
    public class Food : MonoBehaviour
    {
        #region events
        public event Action<int> OnSetHealth = (int i) => { };
        #endregion
        #region private fields
        [SerializeField] int _health;
        [SerializeField]FoodData _data;
        #endregion
        #region private methods
        private void Start()
        {
            if (_data != null)
            {
                Init(_data);
            }
        }
        #endregion
        #region public methods
        public int Health
        {
            get
            {
                return _health;
            }
            set
            {
                _health = value;
                if (_health < 0)
                {
                    _health = 0;
                }
                OnSetHealth(_health);
            }

        }
        public void Init(FoodData data_)
        {
            _data = data_;
            _health = _data.Health;
        }
        public void Damage( int damage_)
        {
            if (GameState.Instance().State != GameStates.PLAY) return;
            Debug.Log(Health);
            Health -= damage_;
            if (Health <= 0)
            {
                Destroy(gameObject);
                GameState.Instance().State = GameStates.GAMEOVER;
            }
        }
        #endregion
    }
}