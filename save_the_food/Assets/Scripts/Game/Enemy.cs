﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using Library; 

namespace SaveTheFood
{
   

    public class Enemy : MonoBehaviour
    {
        #region public fields
        public event Action<int> OnDead = (int i) => { };
        #endregion

        #region private fields
        [SerializeField] GameObject _target = null;
        [SerializeField] EnemyData _data = null;
        [SerializeField] TextMeshProUGUI _scoreText;

        enum EnemyStates { MOVE,EAT,DEAD}
        EnemyStates _enemyState = EnemyStates.MOVE;

        int _health;
        #endregion
        #region private methods
        // Start is called before the first frame update
        void Start()
        {
            if (_target != null && _data != null)
            {
               Init(_data, _target);
            }
        }

        // Update is called once per frame
        void Update()
        {
            if (GameState.Instance().State != GameStates.PLAY) return;
        }
        void move()
        {
            float z = transform.position.z;

            Vector3 tmpPos = Vector2.MoveTowards(transform.position, _target.transform.position, Time.deltaTime * _data.Speed);
            transform.position = new Vector3(tmpPos.x, tmpPos.y, z);
        }
        IEnumerator Move()
        {
            while(_enemyState == EnemyStates.MOVE)
            {
                if (GameState.Instance().State == GameStates.PLAY)
                {
                    if (Vector2.Distance(transform.position, _target.transform.position) < 0.1)
                    {
                        State = EnemyStates.EAT;
                    }
                    move();
                }
                yield return null;
            }
            yield break;
        }

        IEnumerator Dead()
        {
            GetComponent<SpriteRenderer>().sprite = _data.DeadSprite;
            Vector3 tmpPos = transform.position;
            tmpPos.z += 1;
            transform.position = tmpPos;
            yield return new WaitForSeconds(0.5f);
            Destroy(gameObject);
        }
        IEnumerator Eat()
        {
            if (GameState.Instance().State == GameStates.PLAY)
            {
                while (_target != null)
                { 
                    yield return new WaitForSeconds(_data.SpeedEating);
                    Food food;
                    if (_target == null)yield break;
                    if (_target.TryGetComponent<Food>( out food))
                    {
                        food.Damage(_data.Damage);
                     }
                }
            }

            yield return null;
        }

        EnemyStates State
        {
            get
            {
                return _enemyState;
            }
            set
            {
                _enemyState = value;
                StopAllCoroutines();

                switch (_enemyState)
                {
                    case EnemyStates.MOVE:
                        StartCoroutine("Move");
                        break;
                    case EnemyStates.EAT:
                        StartCoroutine("Eat");
                        break;
                    case EnemyStates.DEAD:
                        GetComponent<BoxCollider2D>().enabled = false;
                        _scoreText.text = _data.Score.ToString();
                        OnDead(_data.Score);
                        StartCoroutine("Dead");
                        break;
                }
            }
        }
        private void OnDestroy()
        {

        }
        void Damage(int damage_)
        {
            _health -= damage_;
            if (_health <= 0)
            {
                State = EnemyStates.DEAD;
            }

        }
        private void OnTriggerEnter2D(Collider2D collision)
        {
            if (GameState.Instance().State != GameStates.PLAY) return;
            GameObject tmpObject = collision.gameObject;
            if ( tmpObject.tag == "Food")
            {
                _target = tmpObject;
                State = EnemyStates.EAT;
            }
        }
        private void OnMouseDown()
        {
            if (GameState.Instance().State == GameStates.PLAY)
            {
                Damage(1);
            }
        }
        #endregion
        #region public methods
        public void Init(EnemyData data_, GameObject target_)
        {
            _data = data_;
            _health = data_.Health;
            _target = target_;
            transform.rotation = Quaternion.Euler(0, 0, MatchBase.angleLookRotation(transform.position, transform.up, _target.transform.position));
            GetComponent<SpriteRenderer>().sprite = _data.MainSprite;
            State = EnemyStates.MOVE;
        }
        
        #endregion
    }
}
