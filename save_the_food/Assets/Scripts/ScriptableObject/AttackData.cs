﻿using UnityEngine;
using System;

namespace SaveTheFood
{
    public struct WaveElement { public float time; public EnemyData enemyData; /*public st1[] k;*/ }
    public abstract class AttackData : ScriptableObject
    {
        #region public methods
        public abstract  WaveElement[][] GetAttackWaves(int countPoints, int atackNumber = -1 );
        #endregion
    }
}