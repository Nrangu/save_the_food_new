﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SaveTheFood
{
    [CreateAssetMenu(menuName ="Save the food/FoodData", fileName ="FoodData")]
    public class FoodData : ScriptableObject
    {
        #region private fields
        [Tooltip("За сколько укусов можно съексть еду, тупо количество жизни еды")]
        [SerializeField] int _health;
        #endregion
        #region public properties
        public int Health
        {
            get
            {
                return _health;
            }
        }

        #endregion
    }
}