﻿using System;
using System.Collections.Generic;
using System.Collections;
using UnityEngine;

namespace SaveTheFood
{
    [Serializable] public struct Elem { public int countEnemy; public float time; public EnemyData enemyData; }
    [Serializable] public struct Wave { public Elem[] elems; }
    [Serializable] public struct PointData { public Wave[] waves; };
    [CreateAssetMenu(menuName = "Save the food/ custom attack data", fileName = "CustomAttackData")]
    public class CustomAttackData : AttackData
    {
        #region private fields
        // данные для атаки хранятся так номер точки респауна, внутри точки все волны для данной точки, 
        //а внутри волны данные по волне - промежутки появленя врагов, сколько врагов, и каких врагов
        [SerializeField] PointData[] _pointData;
        #endregion
        #region public methods
        public override WaveElement[][] GetAttackWaves(int countPoints_, int waveNumber_ = -1)
        {
 
            WaveElement[][] waveElements =new  WaveElement[countPoints_][];
            //пока предполагаем что у нас есть данные для всех точек
            // позже сделаем проверку если данных меньше чем точек.
            // предположительно эти данные будем заполнять случайными данными

            // переберам данные по точки респауна
            for (int i = 0; i < _pointData.Length; i++)
            {
                List<WaveElement> elements = new List<WaveElement>();
                // получаем данные для нужной волны
                for (int k = 0; k < _pointData[i].waves[waveNumber_].elems.Length;k++)
                {
                    for(int j =0; j < _pointData[i].waves[waveNumber_].elems[k].countEnemy; j++)
                    {
                        WaveElement tmpElem;
                        tmpElem.enemyData = _pointData[i].waves[waveNumber_].elems[k].enemyData;
                        tmpElem.time = _pointData[i].waves[waveNumber_].elems[k].time;
                        elements.Add(tmpElem);
                    }
                }
                waveElements[i] = elements.ToArray();
            }

            return waveElements;
        }
        #endregion
    }
}