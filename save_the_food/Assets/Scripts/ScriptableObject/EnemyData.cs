﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace SaveTheFood
{
    [CreateAssetMenu(menuName ="Save the food/EnemyData", fileName ="EnemyData")]
    public class EnemyData : ScriptableObject
    {

        #region private fields
        [Tooltip("Основной спрайт врага")]
        [SerializeField] Sprite _mainSprite;

        [Tooltip("Спрайт который появляется после смерти врага")]
        [SerializeField] Sprite _deadSprite;

        [Tooltip("Сколько очков получает игрок за убийство врага")]
        [SerializeField] int _score;

        [Tooltip("Количество жизни врага")]
        [SerializeField] int _health;

        [Tooltip("Скорость перемещения врага")]
        [SerializeField] float _speed;

        [Tooltip("Урон наносимый за один укус")]
        [SerializeField] int _damage;

        [Tooltip("Сколько укусов в секунду делает враг")]
        [SerializeField] float _speedEating;
        #endregion
        #region public properties, methods
        public Sprite MainSprite
        {
            get
            {
                return _mainSprite;
            }
        }

        public Sprite DeadSprite
        {
            get
            {
                return _deadSprite;
            }
        }
        public int Score
        {
            get
            {
                return _score;
            }
        }
        public int Health
        {
            get
            {
                return _health;
            }
        }

        public float Speed
        {
            get
            {
                return _speed;
            }
        }
        public int Damage
        {
            get
            {
                return _damage;
            }
        }

        public float SpeedEating
        {
            get
            {
                return _speedEating;
            }
        }
        #endregion
    }
}