﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SaveTheFood
{
    [CreateAssetMenu(menuName = "Save the food/Random attack data", fileName ="RandomAttackData")]
    public class RandomAttackData : AttackData
    {
        #region private fields
        [SerializeField] float _minTime;
        [SerializeField] float _maxTime;
        [SerializeField] int _minEnemies;
        [SerializeField] int _maxEnemies;
        [SerializeField] EnemyData[] _enemies;

        #endregion
        public override WaveElement[][] GetAttackWaves(int countPoints_, int atackNumber_ = -1)
        {

            WaveElement[][] _elements = new WaveElement[countPoints_][];

            for (int i = 0; i < countPoints_; i++)
            {
                int countEnemies = (int)Random.Range(_minEnemies, _maxEnemies + 1);

                _elements[i] = new WaveElement[countEnemies];

                for(int k =0; k < countEnemies; k++)
                {
                    _elements[i][k].time = Random.Range(_minTime, _maxTime);

                    int number = Random.Range(0, _enemies.Length);

                    _elements[i][k].enemyData = _enemies[number];

                }

            }
            return _elements;

            return null;
        }
    }
}